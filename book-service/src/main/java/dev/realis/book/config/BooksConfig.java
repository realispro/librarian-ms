package dev.realis.book.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class BooksConfig {

    @Bean
    RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
