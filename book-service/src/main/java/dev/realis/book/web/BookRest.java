package dev.realis.book.web;

import dev.realis.book.data.BookRepository;
import dev.realis.book.model.Book;
import dev.realis.book.model.Publisher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
public class BookRest {

    @Autowired
    private BookRepository repository;

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/books")
    public List<Book> getBooks(){
        log.info("about to retrieve all books");
        return repository.findAll();
    }

    @GetMapping("/publishers/{publisherId}/books")
    public List<Book> getBooksByPublisher(@PathVariable int publisherId){
        log.info("about to retrieve books of publisher " + publisherId);
        Publisher publisher = restTemplate
                .exchange("http://localhost:8901/publishers/" + publisherId, HttpMethod.GET, HttpEntity.EMPTY,
                        Publisher.class).getBody();
        return repository.findBookByPublisherId(publisherId).stream().peek(book -> book.setPublisher(publisher))
                .collect(Collectors.toList());
    }

}
