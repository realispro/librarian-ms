package dev.realis.book.data;

import dev.realis.book.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BookRepository extends JpaRepository<Book, Integer> {

    List<Book> findBookByPublisherId(Integer publisherId);
}
