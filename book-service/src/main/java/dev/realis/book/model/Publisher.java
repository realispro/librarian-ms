package dev.realis.book.model;

import lombok.Data;

@Data
public class Publisher {
    private int id;
    private String name;
    private String logo;
}
