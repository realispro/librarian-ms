package dev.realis.book.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String title;

    private String author;

    private String cover;

    private float price;

    private Integer publisherId;

    @Transient
    private Publisher publisher;
}
