package dev.realis.publisher.web;

import dev.realis.publisher.data.PublisherRepository;
import dev.realis.publisher.model.Publisher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
public class PublisherRest {
    
    @Autowired
    private PublisherRepository repository;
    
    @GetMapping("/publishers")
    public List<Publisher> getPublishers(){
        log.info("about to retrieve all publishers");
        return repository.findAll();
    }

    @GetMapping("/publishers/{id}")
    public Publisher getPublisher(@PathVariable int id){
        log.info("about to retrieve publisher {}", id);
        return repository.findById(id).orElse(null);
    }
    
}
